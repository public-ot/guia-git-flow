# Git-Flow:

## Introducción

[Git](http://git-scm.com/) es, sin duda alguna, el sistema de control de versiones más utilizado en la actualidad. Gracias a su tremendo poder y flexibilidad, el trabajo en equipo dejó de ser un dolor de cabeza.

El principal poder de Git reside en su capacidad de generar copias: **_es un clonador serial_**. Gracias a estas copias (**ramas**), podemos generar duplicados exactos de nuestro trabajo e introducir cambios sin miedo a romper nada del original. Si algo falla, podemos volver exactamente al punto de inicio.

Además, es recomendable seguir ciertas convenciones que permiten ahorrar muchos problemas y facilitarán nuestro trabajo. Para ello, existe una herramienta que pone en práctica y automatiza estas convenciones, y simplifica aún más el trabajo: **[GIT-FLOW](https://danielkummer.github.io/git-flow-cheatsheet/index.es_ES.html).**

> _Git-flow es un conjunto de extensiones para Git, basado en_ [_el modelo de ramificaciones de_ **_Vincent Driessen_**](http://nvie.com/posts/a-successful-git-branching-model/)**_,_** _que nos facilita el trabajo con nuestros repositorios._

Básicamente, _git-flow_ agrega comandos de alto nivel que usan los comandos tradicionales de Git.

## Master y Develop

El centro en torno al cual gira  _git-flow_  son las ramas o branch, y si hay ramas que merecen destacarse, esas son _master_ y _develop_. Todo lo que hagamos como desarrolladores terminará, tarde o temprano, en alguna de estas ramas:

-   **Develop**: La rama que contiene las features a incluir en una próxima salida a producción (pasando previamente por QA). Todos los desarrollos que hagamos — nuevos módulos, refactorings, entre otros. — se guardarán en  _Develop_, a la espera de salir a producción.
-   **Master**: La rama que contiene el código que está en producción. A esta rama, sólo debe llegar código que está en — o está listo para estar en — producción.

### Master y Develop, ¡Ambas son intocables!

Nunca  se deberia de modificar código directamente en alguna de estas dos ramas.

![develop-master](./assets/develop-master.svg)

## Los 3 elementos básicos de git-flow

Para trabajar con  _git-flow_, hay tres conceptos importantes que aprender:  _feature_,  _hotfix_  y  _release_.

### 1. Feature

> _Una feature no es otra cosa que una rama de Git con una copia exacta del contenido de Develop_.

Para poder realizar cambios sobre lo que está en _develop_, es necesario generar una copia exacta de su contenido y se coloca en una nueva rama llamada _feature_.

El propósito de las  _features_  es desarrollar nuevas funcionalidades del proyecto, en un ambiente controlado, de forma tal de que, una vez que estén listos para agregarse a una futura versión, se agreguen a  _develop_.

Y hacerlo de esta forma tiene dos ventajas principales:

#### **I. No trabajamos sobre _Develop_**

> Consideremos un escenario en el que un grupo de personas trabaja colaborativamente sobre un proyecto. El encargado del proyecto te asigna el desarrollo de un nuevo módulo y tú le dedicas varias jornadas de trabajo. Justo unos días antes de terminar el módulo, el encargado del proyecto decide que, finalmente, ese módulo no será necesario.

> Si hubiésemos trabajado directamente sobre Develop, borrar esos cambios probablemente generaría muchos conflictos y rompería el código de algún otro miembro del equipo que se haya descargado los cambios. Además, seguramente tendríamos que eliminar o comentar código manualmente, lo que empeoraría aún más la situación.

> Por el contrario, si trabajamos en una rama aislada o feature, simplemente podemos descartar esa rama, sin afectar a Develop. O bien, podríamos dejarla en stand by por si en un futuro próximo vuelve a retomarse la idea de integrar el módulo en cuestión.

#### **II. Logramos más estabilidad**

> Al usar una rama separada, los desarrollos están más controlados y pasan por una serie de revisiones más minuciosas antes de  _mergearse_  con Develop. Imagina que si todos trabajasen sobre la misma rama, podrían generarse conflictos: dos personas modificando la misma porción de código o alguien borrando cosas que otro necesitaba.

> Con las features, nos aseguramos que los desarrollos están controlados y no vamos a afectar el trabajo de otro miembro del equipo. Sumado a esto, si trabajamos bien, las features deberían atravesar una serie de pruebas exhaustivas antes de  _mergearse_  a Develop.

Los nombres de todas las features comienzan con la palabra  _feature_  seguido de una barra invertida y la referencia que nosotros querramos usar para identificarla.

![develop-master-feature](./assets/develop-master-feature.svg)

### 2. Hotfix

> _Un hotfix es una rama con una copia exacta del contenido de_ master.

Para poder realizar cambios sobre lo que está en  _master_  y corregir un bug en producción, es necesario generar una copia exacta de su contenido y lo colocarlo en una nueva rama de tipo _hotfix_. De esta forma, se evita trabajar directamente sobre el código que está en  _master_, y se sigue trabajando en un “ambiente” controlado.

Cuando estemos seguros de haber corregido el bug, podemos cerrar el hotfix para que se agregue a _develop_ y _master_.

**Siempre que cerremos un hotfix, el merge se debe hacer con  _master_  y con  _develop_.**

Al igual que ocurre con las features, los nombres de todos los _hotfix_ comienzan con la palabra _hotfix_ seguido de una barra invertida. A continuación, se agrega la referencia.

![develop-master-feature-release-hotfix](./assets/develop-master-feature-release-hotfix.svg)

### 3. Release

> Una release es una rama con una copia exacta del contenido de _develop_, que está lista (o casi) para ser subida a Producción. Una _release_ es el pasaje de _develop_ a _master_.

Al generar la _release_, que no es otra cosa que una copia de develop, esta se publica en un entorno de pruebas. La idea es someterla a unas últimas revisiones antes de mandarla a producción.

Ahora, supóngase que entre dichas revisiones se encuentra un bug menor que surge de los nuevos cambios, y que es mejor que no llegue a Producción. Entonces, lo que hacemos es solucionar el bug directamente en la rama _release_ y se vuelven a ejecutar la pruebas.

**Cuando todo este OK para subirse a Producción, se fusiona la _release_ con _develop_ y _master_.**

![develop-master-feature-release](./assets/develop-master-feature-release.svg)

## Identificación de Branch en Git-Flow

Para mantener una estructura ordenada y poder dar un facil seguimiento a las modificaciones realaizadas en los proyectos, la identiciación de las ramas _git-flow_ se realiza de la siguiente manera:

- Como se ha mencionada con anterioridad, se inicia indicando el tipo de rama: _feature_, _hotfix_ o _release_, seguido de una barra invertida.
- Posteriormente se agregan en **Mayúsculas** las iniciales de la persona encargada en el desarrollo de la actividad, seguidas por una nueva barra invertida.
- Finalmente, se agrega en **Mayúsculas** el prefijo de la actividad.

### Ejemplos de Git-Flow Branch

- **Master**: `master`
- **Develop**: `develop`
- **Feature**: `feature/JM/MV-001`
- **Hotfix**: `hotfix/JM/MV-002`
- **Release**: `release/JM/MV-003`

## Mensajes en Commits

Todos los mensajes en los commits deben de contener como prefijo el nombre de la branch sobre la cual se esta trabajando. Por Ejemplo: `[feature/JM/MV-001] - AuthController done`

### Automatizar Commits por Branchs

Puedes agregar este hook a Git para que no te preocupes en agregar el prefijo para cada commit:

`nano .git/hooks/prepare-commit-msg`

con el siguiente contenido:

```bash
#!/bin/sh
cat $1 > /tmp/__tmp_commit_msg
PREFIX="[`git rev-parse --abbrev-ref HEAD`]"
ACTUAL=`cat /tmp/__tmp_commit_msg`
echo "$PREFIX - $ACTUAL" > $1
```

Recuerda que para realizar esto, debes encontrarte en el directorio del repositorio local. Ademas, establecer al archivo creado como un ejecutable:

`chmod +x .git/hooks/prepare-commit-msg`

Y por ultimo, establece NANO como tu editor de commits, sino entonces el hook le devolvera un error.

`git config --global core.editor /usr/bin/nano`

Cabe destacar que si usted desea usar otro editor como vi, es permitido.

## Instalación de la Extensión Git flow

El proceso de instalación de git-flow es sencillo. Los paquetes para git-flow están disponibles en múltiples sistemas operativos. En los sistemas OSX, se puede ejecutar `brew install git-flow`. En Windows necesitará [descargar e instalar git-flow](https://git-scm.com/download/win). Después de instalar git-flow puedes usarlo en tu proyecto ejecutando `git flow init`. Git-flow es una envoltura alrededor de Git. El comando `git flow init` es una extensión del comando [`git init`](https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-init) predeterminado, el cual crea por defecto las ramas `master` y `develop`.

### Cómo funciona

#### Crear y dominar las ramas _develop_ y _master_

En lugar de una sola rama `master`, este flujo de trabajo utiliza dos ramas para registrar el historial del proyecto. La la rama `master` almacena el historial de producción de la aplicación y la `develop` sirve como una rama de integración para las `features`.

El primer paso es integrar el valor predeterminado de la rama `master` a una rama `develop`. Una forma sencilla de hacer esto es que un desarrollador cree una `develop` vacía localmente y la envíe al servidor:

```bash
$ git branch develop
$ git push -u origin develop
```

Esta rama contendrá el historial completo del proyecto, mientras que la rama `master` contendrá una versión abreviada.

Cuando se utiliza la biblioteca de extensión `git-flow`, la ejecución del comando `git flow init` en un repositorio existente creará la rama `develop`:

```bash
$ git flow init

Which branch should be used for bringing forth production releases?
   - master
Branch name for production releases: [master]
Branch name for "next release" development: [develop]

How to name your supporting branch prefixes?
Feature branches? [] feature/
Bugfix branches? [] bugfix/
Release branches? [] release/
Hotfix branches? [] hotfix/
Support branches? [] support/
Version tag prefix? []
Hooks and filters directory?

$ git branch
* develop
 master
```

#### Creando una rama _feature_

Sin las extensiones de git-flow:

```bash
$ git checkout develop
$ git checkout -b feature_branch
```

Al usar la extensión git-flow:

```bash
$ git flow feature start feature_branch
```

##### Finalizar una rama _feature_

Cuando se haya terminado con el trabajo de desarrollo en la features, el siguiente paso es fusionar el `feature_branch` a `develop`.

Sin las extensiones de git-flow:

```bash
$ git checkout develop
$ git merge feature_branch
```

Usando las extensiones de git-flow:

```bash
$ git flow feature finish feature_branch
```

**NOTA: FINALIZAR DIRECTAMENTE LAS FEATURES NO ES RECOMENDABLE YA QUE LAS ESTAS PRIMERO DEBEN DE PASAR POR UN PROCESO DE CODE REVIEW Y QA ANTES DE SER INTEGRADAS A LA RAMA DEVELOP.**

#### Creando una rama _release_

Al igual que las ramas `feature`, las ramas `release` se basan en la rama `develop`. Se pueden crear una nueva rama `release` usando los siguientes comandos.

```bash
$ git checkout develop
$ git checkout -b release/0.1.0
```

Al usar las extensiones git-flow:

```bash
$ git flow release start 0.1.0
Switched to a new branch 'release/0.1.0'
```

Para terminar una `release`, se usan los siguientes comandos:

Sin las extensiones de git-flow:

```bash
$ git checkout develop
$ git merge release/0.1.0
$ git checkout master
$ git checkout merge release/0.1.0
```

O con la extensión git-flow:

```bash
$ git flow release finish '0.1.0'
```

**NOTA: FINALIZAR DIRECTAMENTE LAS RELEASE NO ES RECOMENDABLE YA QUE LAS ESTAS PRIMERO DEBEN DE PASAR POR UN PROCESO DE CODE REVIEW Y QA ANTES DE SER INTEGRADAS A LA RAMAS MASTER Y DEVELOP.**

#### Creando una rama _hotfix_

Sin las extensiones de git-flow:

```bash
$ git checkout master
$ git checkout -b hotfix_branch
```

Al usar las extensiones git-flow:

```bash
$ git flow hotfix start hotfix_branch
```

Similar a terminar una rama `release`, una rama `hotfix` se fusiona en ambas ramas `master` y `develop`.

```bash
git checkout master
git merge hotfix_branch
git checkout develop
git merge hotfix_branch
git branch -D hotfix_branch
```

O al usar las extensiones git-flow:

```bash
$ git flow hotfix finish hotfix_branch
```

**NOTA: FINALIZAR DIRECTAMENTE LAS HOTFIX NO ES RECOMENDABLE YA QUE LAS ESTAS PRIMERO DEBEN DE PASAR POR UN PROCESO DE CODE REVIEW Y QA ANTES DE SER INTEGRADAS A LA RAMAS MASTER Y DEVELOP.**

## Flujo General de Git-Flow

1. Se crea una rama `develop` desde la rama `master`.
2. Se crea una rama `release` desde la rama `develop`.
3. Las ramas `feature` se crean a partir de la rama `develop`.
4. Cuando una `feature` ha sido completada, se fusiona en la rama `develop`.
5. Cuando se finaliza una rama `release`, esta se fuciona en las ramas `develop` y `master`.
6. Si en la rama `master` se detecta un bug, se crea una rama `hotfix` a partir de la `master`.
7. Una vez que se termina el `hotfix`, se fusiona a las ramas `develop` y `master`.
